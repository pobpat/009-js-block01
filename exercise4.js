var age = 29;
var end_age = 99;
teas_day = 2;

var howManyTeas = function(age, end_age, teas_day){
    return (end_age - age) * 365 * teas_day
}

module.exports = {
    howManyTeas,
    age,
    end_age,
    teas_day
}