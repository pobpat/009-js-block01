var celsius = 28;
var fahrenheit = 78;
var toCelsius = function (fahrenheit){
    return Math.round((fahrenheit - 32) * 5 / 9)
}
var toFahr = function (celsius){
    return Math.round(celsius / 5 * 9 + 32)
}

module.exports = {
    celsius, 
    fahrenheit, 
    toCelsius, 
    toFahr,
}